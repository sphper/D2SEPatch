#给D2SE启动游戏.exe打补丁，增加1.13d/1.14a/1.14b/1.14c/1.14d五个版本
import sys, traceback
from patch_api import hack_dll_batch, patch_path, target_path, patch_file
import subprocess
from shutil import copyfile

try:
    filename = "%s\\..\\..\\D2SE启动游戏.exe" % target_path
    hack_bin = [0xE8, 0x99, 0xD4]
    hack_dll_batch(filename, hack_bin, 0x3102)
    hack_bin = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
    hack_dll_batch(filename, hack_bin, 0x10B12)

    child = subprocess.run("%s\\D2SE\\privil.bat %s" % (patch_path, filename), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)  #设置exe的兼容性和管理员权限
except:
    print("Unexpected error:", sys.exc_info())
    print(traceback.format_exc())
    exit(1)